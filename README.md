## [LibreNote](http://librenote.aerobatic.io)

Répertoire pour [LibreNote](http://librenote.aerobatic.io), un projet de site web et de wikis entiérement basé sur [TiddlyWiki](http://tiddlywiki.com/).

Le site LibreNote est accessible à l'adresse http://librenote.aerobatic.io. Il regroupe une collection de wikis sur les logiciels libres.
 
### Les Wikis
* [LinuxNote](http://librenote.aerobatic.io/linuxnote) - Un guide sur Chakra, KDE et les Logiciels Libres

* [TiddlyNote](http://librenote.aerobatic.io/tiddlynote) - Une introduction à TiddlyWiki

* [GitNote](http://librenote.aerobatic.io/gitnote) - Les bases de Git